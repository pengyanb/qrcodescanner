import Expo from 'expo';
import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { NavigationProvider, StackNavigation } from '@expo/ex-navigation';
import { FontAwesome } from '@expo/vector-icons';

import Router from './navigation/Router';
import cacheAssetsAsync from './utilities/cacheAssetsAsync';
import {initAppParamFromStorage, persistAppParamToStorage} from './utilities/PersistDataHelper';

import {connect, Provider} from 'react-redux';
import {createStore} from 'redux';

const store = createStore((state={}, action)=>{
  switch(action.type){
    case "refreshQrCodeList":
      return {
        refreshQrCodeList: action.flag,
      };
    case "needToShowScanner":
      return {
        needToShowScanner: action.flag,
      };
    default: return state;
  }
});

class AppContainer extends React.Component {
  state = {
    appIsReady: false,
  };

  async componentWillMount() {
    this._loadAssetsAsync();
    let appParam = await initAppParamFromStorage();
    console.log("[appParam]: " + JSON.stringify(appParam));
    if(appParam === null){
      await persistAppParamToStorage({playSound:true, vibrate:true, qrCodeSet:[]});
    }
    StatusBar.setHidden(true);
  }

  async _loadAssetsAsync() {
    try {
      await cacheAssetsAsync({
        images: [require('./assets/images/expo-wordmark.png')],
        fonts: [
          FontAwesome.font,
          { 'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf') },
        ],
      });
    } catch (e) {
      console.warn(
        'There was an error caching assets (see: main.js), perhaps due to a ' +
          'network timeout, so we skipped caching. Reload the app to try again.'
      );
      console.log(e.message);
    } finally {
      this.setState({ appIsReady: true });
    }
  }

  render() {
    if (this.state.appIsReady) {
      return (
        <Provider store={store}>
            <View style={styles.container}>
            <NavigationProvider router={Router}>
              <StackNavigation
                id="root"
                initialRoute={Router.getRoute('rootNavigation')}
              />
            </NavigationProvider>

            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
            {Platform.OS === 'android' &&
              <View style={styles.statusBarUnderlay} />}
          </View>
        </Provider>
      );
    } else {
      return <Expo.AppLoading />;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  statusBarUnderlay: {
    height: 24,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
});

Expo.registerRootComponent(AppContainer);
