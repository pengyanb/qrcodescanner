import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Notifications } from 'expo';
import {
  StackNavigation,
  TabNavigation,
  TabNavigationItem,
} from '@expo/ex-navigation';
import { FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons';

import Alerts from '../constants/Alerts';
import Colors from '../constants/Colors';
// import registerForPushNotificationsAsync
//   from '../api/registerForPushNotificationsAsync';
import {connect} from 'react-redux';

class RootNavigation extends React.Component {
  componentDidMount() {
    //this._notificationSubscription = this._registerForPushNotifications();
  }

  componentWillUnmount() {
    //this._notificationSubscription && this._notificationSubscription.remove();
  }

  render() {
    return (
      <TabNavigation tabBarHeight={50} id="main" navigatorUID="main" initialTab="qrcodescan">
        <TabNavigationItem
          id="history"
          onPress={()=>{
            //console.log("history tab pressed");
            this.props.navigation.performAction(({tabs, stacks})=>{
              this.props.reduxDispatch.setNeedRefreshQrCodeList(true);
              tabs('main').jumpToTab('history');
            });
          }}
          renderIcon={isSelected => this._renderIcon('clipboard-text', isSelected, "MaterialCommunityIcons")}>
          <StackNavigation id="history" navigatorUID="history" initialRoute="history" />
        </TabNavigationItem>

        <TabNavigationItem
          id="qrcodescan"
          onPress={()=>{
            this.props.navigation.performAction(({tabs, stacks})=>{
              this.props.reduxDispatch.setNeedToShowScanner(true);
              tabs('main').jumpToTab('qrcodescan');
            });
          }}
          renderIcon={isSelected => this._renderIcon('qrcode-scan', isSelected, "MaterialCommunityIcons")}>
          <StackNavigation id="qrcodescan" navigatorUID="qrcodescan" initialRoute="qrcodescan" />
        </TabNavigationItem>

        <TabNavigationItem
          id="settings"
          renderIcon={isSelected => this._renderIcon('cog', isSelected)}>
          <StackNavigation id="settings" navigatorUID="settings" initialRoute="settings" />
        </TabNavigationItem>
      </TabNavigation>
    );
  }

  _renderIcon(name, isSelected, iconLibraryName="FontAwesome") {
    switch(iconLibraryName){
      case "MaterialCommunityIcons":
        return(<MaterialCommunityIcons
                name={name}
                size={32}
                color={isSelected ? Colors.tabIconSelected : Colors.tabIconDefault}
              />);
      case "FontAwesome":
        return (
          <FontAwesome
            name={name}
            size={32}
            color={isSelected ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
        );
      default: return null;
    }
    
  }

  // _registerForPushNotifications() {
  //   // Send our push token over to our backend so we can receive notifications
  //   // You can comment the following line out if you want to stop receiving
  //   // a notification every time you open the app. Check out the source
  //   // for this function in api/registerForPushNotificationsAsync.js
  //   registerForPushNotificationsAsync();

  //   // Watch for incoming notifications
  //   this._notificationSubscription = Notifications.addListener(
  //     this._handleNotification
  //   );
  // }

  // _handleNotification = ({ origin, data }) => {
  //   this.props.navigator.showLocalAlert(
  //     `Push notification ${origin} with data: ${JSON.stringify(data)}`,
  //     Alerts.notice
  //   );
  // };
}

const mapDispatchToProps = (dispatch)=>{
  return {reduxDispatch:{
    setNeedRefreshQrCodeList: (flag)=>{dispatch({type:'refreshQrCodeList', flag:flag})},
    setNeedToShowScanner:(flag)=>{dispatch({type:'needToShowScanner', flag:flag})},
  }};
};
export default connect(null, mapDispatchToProps)(RootNavigation);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  selectedTab: {
    color: Colors.tabIconSelected,
  },
});
