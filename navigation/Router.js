import { createRouter } from '@expo/ex-navigation';

import HistoryScreen from '../screens/HistoryScreen';
import QrCodeScanScreen from '../screens/QrCodeScanScreen';
import SettingsScreen from '../screens/SettingsScreen';
import RootNavigation from './RootNavigation';

export default createRouter(() => ({
  history: () => HistoryScreen,
  qrcodescan: () => QrCodeScanScreen,
  settings: () => SettingsScreen,
  rootNavigation: () => RootNavigation,
}));
