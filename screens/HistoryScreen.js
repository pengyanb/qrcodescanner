import React from 'react';
import {
  Image,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Dimensions,
} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import validUrl from 'valid-url';
import Expo from 'expo';
import { MonoText } from '../components/StyledText';
import {initAppParamFromStorage, persistAppParamToStorage} from '../utilities/PersistDataHelper';
import Alerts from '../constants/Alerts';

import {connect} from 'react-redux';

class HistoryScreen extends React.Component {
  static route = {
    navigationBar: {
      visible: false,
    },
  };
  constructor(props){
    super(props);
    this.state = {qrCodeInfoArray:[], selectedIndex: -1};
  }
  componentWillMount(){
    this._initQrCodeInfoArrayState();
  }

  componentDidUpdate(){
    if(this.props.reduxState.needToRefreshQrCodeList){
      this.props.reduxDispatch.setNeedRefreshQrCodeList(false);
      this.setState({selectedIndex:-1});
      this._initQrCodeInfoArrayState();
    }
  }

  async _initQrCodeInfoArrayState(){
    let appParam = await initAppParamFromStorage();
    if(appParam.hasOwnProperty('qrCodeSet') && Array.isArray(appParam.qrCodeSet)){
      let qrCodeArray = Array.from(appParam.qrCodeSet);
      let qrCodeInfoArray = [];
      for(let i=0; i<qrCodeArray.length; i++){
        qrCodeInfoArray.push({key:i, qrCode: qrCodeArray[i]});
      }
      this.setState({qrCodeInfoArray: qrCodeInfoArray});
    }
  }
  render() {
    //console.log("route.params.refreshList: " + JSON.stringify(this.props.route.params));
    return (
      <FlatList 
        style={[StyleSheet.absoluteFill, {backgroundColor:'#455A64', paddingTop:64}]}
        contentContainerStyle={{alignItems:'center'}}
        data={this.state.qrCodeInfoArray}
        renderItem = {({item})=>{
          return(
            <View style={{flexDirection:'column', marginTop:20}}>
              <TouchableOpacity style={{backgroundColor:'white',
                            borderRadius:10,
                            padding:10,
                            shadowColor:'#000000',
                            shadowOffset:{width:4, height:8},
                            shadowRadius:5,
                            shadowOpacity:0.8,
                            flexDirection:'row',
                            alignItems:'center',
                            justifyContent:'center',
                            width:Dimensions.get('window').width*0.9,
                }}
                onPress={()=>{
                  if(this.state.selectedIndex == item.key){
                    this.setState({selectedIndex: -1});
                  }
                  else{
                    this.setState({selectedIndex: item.key});
                  }
                }}>
                <QRCode value={item.qrCode} style={{flex:1}} />
                <Text style={{flex:3, paddingHorizontal:4}}>{item.qrCode}</Text>
              </TouchableOpacity>
              {(this.state.selectedIndex == item.key) ?
              (<View style={{backgroundColor:'rgba(0,0,0,0)',
                            paddingTop:5, paddingHorizontal:10, paddingBottom:10,
                            flexDirection:'row', alignItems:'center', justifyContent:'center',
                            width:Dimensions.get('window').width*0.9}}>
                <TouchableOpacity style={{flex:1, alignItems:'center', justifyContent:'center',
                                          margin:5, borderRadius:5, backgroundColor:'#FFFFFF',
                                          shadowColor:'#000000', shadowOffset:{width:4, height:8}, shadowOpacity:0.8, shadowRadius:5,
                                          height:Dimensions.get('window').width*0.12}}
                  onPress={()=>{
                    if(validUrl.isWebUri(item.qrCode)){
                      Expo.WebBrowser.openBrowserAsync(item.qrCode);
                    }
                    else{
                      this.props.navigator.showLocalAlert('Invalid URL' ,Alerts.error);
                    }
                  }}>
                  <Text style={{textAlign:'center',}}>Open</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flex:1, alignItems:'center', justifyContent:'center',
                                          margin:5, borderRadius:5, backgroundColor:'#F44336',
                                          shadowColor:'#000000', shadowOffset:{width:4, height:8}, shadowOpacity:0.8, shadowRadius:5,
                                          height:Dimensions.get('window').width*0.12}}
                    onPress={async ()=>{
                      let appParam = await initAppParamFromStorage();
                      if(appParam.hasOwnProperty('qrCodeSet') && Array.isArray(appParam.qrCodeSet)){
                        let qrCodeSet = new Set(appParam.qrCodeSet);
                        qrCodeSet.delete(item.qrCode);
                        await persistAppParamToStorage({qrCodeSet:Array.from(qrCodeSet)});
                        let qrCodeArray = Array.from(qrCodeSet);
                        let qrCodeInfoArray = [];
                        for(let i=0; i<qrCodeArray.length; i++){
                          qrCodeInfoArray.push({key:i, qrCode: qrCodeArray[i]});
                        }
                        this.setState({selectedIndex:-1});
                        this.setState({qrCodeInfoArray: qrCodeInfoArray});
                      }
                    }}>
                  <Text style={{textAlign:'center', color:'#FFFFFF'}}>Delete</Text>
                </TouchableOpacity>
              </View>) : null}
            </View>
            );
        }}
        listEmptyComponent={()=>{
          return(<Text>No QR code scanned yet.</Text>);
        }}
        ListFooterComponent={()=>{
          return(<View style={{backgroundColor:'rgba(0,0,0,0)', height:100,}}></View>);
        }}
      />
    );
  }
}

const mapStateToProps = (state)=>{
  console.log(JSON.stringify(state));
  return {reduxState:{
    needToRefreshQrCodeList: state.refreshQrCodeList
  }};
};

const mapDispatchToProps = (dispatch)=>{
  return {reduxDispatch:{
    setNeedRefreshQrCodeList: (flag)=>{dispatch({type:'refreshQrCodeList', flag:flag})},
  }};
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryScreen);
