import React from 'react';
import { 
  Text, 
  View, 
  StyleSheet,
  Dimensions,
  Animated,
  Easing,
  Image,
  TouchableOpacity,
  Vibration,
} from 'react-native';
import Expo, {Permissions, Svg} from 'expo';
import { ExpoLinksView } from '@expo/samples';
import QRCode from 'react-native-qrcode-svg';
import {initAppParamFromStorage, persistAppParamToStorage} from '../utilities/PersistDataHelper';
import {connect} from 'react-redux';

class QrCodeScanScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      hasCameraPermission: null,
      linePos: new Animated.Value(0),
      containerHeight: new Animated.Value(Dimensions.get('window').height),
      qrCodeDetected : false,
      qrCodeContent: "",
    };
  }
  static route = {
    navigationBar: {
      title: 'QR Code Scanner',
      visible: false,
    },
  };

  async componentWillMount(){
    const {status} = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({hasCameraPermission: status === 'granted'});
    this._startAnimation();
    
    this._soundObject = new Expo.Audio.Sound();
    try{
      await this._soundObject.loadAsync(require('../assets/sounds/detection.mp3'));
    }
    catch(err){
      console.log(err);
    }
  }
  componentDidUpdate(){
    if(this.props.reduxState.needToShowScanner){
      this.props.reduxDispatch.setNeedToShowScanner(false);
      this.setState({qrCodeDetected:false});
      this.setState({qrCodeContent:""});
      //this._startAnimation();
    }
  }

  render() {
    const {hasCameraPermission} = this.state;
    if(hasCameraPermission === null){
      return <View />
    }
    else if(hasCameraPermission === false){
      return <Text>No access to camera</Text>;
    }
    else{
      const svgWitdh = Dimensions.get('window').width*0.7;
      if(!this.state.qrCodeDetected){
        return(
          <View style={{flex:1, flexDirection:'column', alignItems:'center', justifyContent:'center'}}>
              <Expo.BarCodeScanner 
                onBarCodeRead={this._handleBarCodeRead}
                style={StyleSheet.absoluteFill}
              />
              <Animated.View style={{position:'absolute', bottom:Animated.multiply(this.state.containerHeight, this.state.linePos), left:0, flexDirection:'column', alignItems:'center', justifyContent:'flex-end'}}>
                <Image style={{width: Dimensions.get('window').width, height:10}} source={require('../assets/images/ScannerLine.png')}></Image>
              </Animated.View>
              <Svg 
                width={svgWitdh} 
                height={svgWitdh} 
                style={{backgroundColor:'rgba(0,0,0,0)'}}>
                <Svg.Polyline
                  points={"0,"+(svgWitdh*0.2)+" 0,0 "+(svgWitdh*0.2)+",0"}
                  fill="none"
                  stroke="white"
                  strokeWidth="10"
                  strokeLinecap="round"
                /> 
                <Svg.Polyline
                  points={""+svgWitdh+","+(svgWitdh*0.2)+" "+svgWitdh+",0 "+(svgWitdh*0.8)+",0"}
                  fill="none"
                  stroke="white"
                  strokeWidth="10"
                  strokeLinecap="round"
                /> 
                <Svg.Polyline
                  points={"0,"+(svgWitdh*0.8)+" 0,"+svgWitdh+" "+(svgWitdh*0.2)+","+svgWitdh}
                  fill="none"
                  stroke="white"
                  strokeWidth="10"
                  strokeLinecap="round"
                />
                <Svg.Polyline
                  points={""+svgWitdh+","+(svgWitdh*0.8)+" "+svgWitdh+","+svgWitdh+" "+(svgWitdh*0.8)+","+svgWitdh}
                  fill="none"
                  stroke="white"
                  strokeWidth="10"
                  strokeLinecap="round"
                />    
              </Svg>
              <Expo.KeepAwake/>
            </View>);
      }
      else{
        return(
          <View
            style={[StyleSheet.absoluteFill,{backgroundColor:'#455A64', flexDirection:'column', alignItems:'center', justifyContent:'center'}]}>
            <View style={{backgroundColor:'white', 
                        borderRadius:10, 
                        padding:10, 
                        shadowColor:'#000000',
                        shadowOffset:{width:4, height:8},
                        shadowRadius:5,
                        shadowOpacity:0.8,
                        flexDirection:'row',
                        alignItems:'center',
                        justifyContent:'center',
                        width:Dimensions.get('window').width*0.9, 
                        }}>
              <QRCode
                value={this.state.qrCodeContent}
                style={{flex:1}}
              />
              <Text style={{flex:3, paddingHorizontal:4}}>{this.state.qrCodeContent}</Text>
            </View>
            <View style={{backgroundColor:'rgba(0,0,0,0)', 
                          padding:10,
                          flexDirection:'row',
                          alignItems:'center',
                          justifyContent:'center',
                          width:Dimensions.get('window').width*0.9,
                        }}>
              <TouchableOpacity style={{
                flex:1, 
                alignItems:'center',
                justifyContent:'center',
                margin:5,
                borderRadius:5, 
                backgroundColor:'#FFFFFF',
                shadowColor:'#000000',
                shadowOffset:{width:4, height:8},
                shadowRadius:5,
                shadowOpacity:0.8,
                height:Dimensions.get('window').width*0.12,
              }}
                onPress={()=>{
                  Expo.WebBrowser.openBrowserAsync(this.state.qrCodeContent);
                }}>
                <Text style={{textAlign:'center',}}>Open</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{
                flex:1, 
                alignItems:'center',
                justifyContent:'center',
                margin:5,
                borderRadius:5, 
                backgroundColor:'#FFFFFF',
                shadowColor:'#000000',
                shadowOffset:{width:4, height:8},
                shadowRadius:5,
                shadowOpacity:0.8,
                height:Dimensions.get('window').width*0.12,
              }}
                onPress={()=>{
                  this.setState({qrCodeContent:''});
                  this.setState({qrCodeDetected:false});
                  //this._startAnimation();
                }}
                >
                <Text style={{textAlign:'center',}}>Rescan</Text>
              </TouchableOpacity>
            </View>
            <Expo.KeepAwake/>
          </View>
        );
      }
    }
  }

  _handleBarCodeRead = async (data)=>{
    if(data.hasOwnProperty('type') && data.type==="org.iso.QRCode" && data.hasOwnProperty("data")){
      this.setState({qrCodeContent:data.data});
      this.setState({qrCodeDetected:true});
      let appParam = await initAppParamFromStorage();
      if(appParam.hasOwnProperty('qrCodeSet')){
        let qrCodeSet;
        if(Array.isArray(appParam.qrCodeSet)){
          qrCodeSet = new Set(appParam.qrCodeSet);
        }
        else{
          qrCodeSet = new Set();
        }
        qrCodeSet.add(data.data);
        console.log("New Array: " + JSON.stringify(Array.from(qrCodeSet)));
        await persistAppParamToStorage({qrCodeSet:Array.from(qrCodeSet)});
        if(appParam.vibrate){
          Vibration.vibrate();
        }
        if(appParam.playSound){
          try{
            await this._soundObject.setPositionAsync(0);
            await this._soundObject.playAsync();
          }
          catch(err){
            console.log(err);
          }
        }
        
      }
    }
  }
  _startAnimation(){
    if(typeof this._animatedSequence !== typeof undefined){
      this._animatedSequence.stop();
    }
    this._animatedSequence = Animated.sequence([
      Animated.timing(this.state.linePos,{
        toValue: 1,
        duration: 3000,
        easing: Easing.inOut(Easing.ease),
      }),
      Animated.timing(this.state.linePos, {
        toValue: 0,
        duration: 0,
      })
    ]).start(()=>{
      //console.log('startAnimation');
      // if(!this.state.qrCodeDetected){
        
      // }
      this._startAnimation();
    });
  }
}

const mapStateToProps = (state)=>{
  return {reduxState:{
    needToShowScanner: state.needToShowScanner,
  }};
};

const mapDispatchToProps = (dispatch)=>{
  return {reduxDispatch:{
    setNeedToShowScanner: (flag)=>{dispatch({type:'needToShowScanner', flag:flag})}
  }};
}

export default connect(mapStateToProps, mapDispatchToProps)(QrCodeScanScreen);
