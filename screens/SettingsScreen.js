import React from 'react';
import { ScrollView, View, Text, StyleSheet, Dimensions, Switch, TextInput} from 'react-native';
import {initAppParamFromStorage, persistAppParamToStorage} from '../utilities/PersistDataHelper';
import QRCode from 'react-native-qrcode-svg';
export default class SettingsScreen extends React.Component {
  static route = {
    navigationBar: {
      title: 'Settings',
      visible:false,
    },
  };

  constructor(props){
    super(props);
    this.state = {
      playSound:true,
      vibrate:true,
      qrCodeContent:'',
    };
  }
  componentWillMount(){
    this._initSettingState();
  }
  async _initSettingState(){
    let appParam = await initAppParamFromStorage();
    if(appParam.hasOwnProperty('playSound')){
      this.setState({playSound:appParam.playSound});
    }
    if(appParam.hasOwnProperty('vibrate')){
      this.setState({vibrate:appParam.vibrate});
    }
  }

  render() {
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={Object.assign({},this.props.route.getContentContainerStyle(), {alignItems:'center'})}>
        <View style={{flexDirection:'column', marginTop:20,
                      backgroundColor:'white',
                      borderRadius:10,
                      padding:10,
                      shadowColor:'#000000',
                      shadowOffset:{width:4, height:8},
                      shadowRadius:5,
                      shadowOpacity:0.8,
                      alignItems:'center',
                      justifyContent:'center',
                      width:Dimensions.get('window').width*0.9,
                    }}>
          <Text style={{color:"#607D8B", fontFamily:'awesome', fontSize:20, fontWeight:'bold', padding:5, fontFamily:'awesome'}}>Settings</Text>
          <View style={{flexDirection:'row', marginTop:20, alignItems:'center', width:Dimensions.get('window').width*0.9}}>
            <Text style={{color:"#607D8B", fontFamily:'awesome',flex:3, fontSize:16, padding:10}}>Play Sound</Text>
            <Switch 
              style={{flex:1}}
              value={this.state.playSound}
              onValueChange={async (value)=>{
                this.setState({playSound:value});
                await persistAppParamToStorage({playSound:value});
              }}
            />
          </View>

          <View style={{flexDirection:'row', marginTop:20, alignItems:'center', width:Dimensions.get('window').width*0.9}}>
            <Text style={{color:"#607D8B", fontFamily:'awesome', flex:3, fontSize:16, padding:10}}>Vibration</Text>
            <Switch 
              style={{flex:1}}
              value={this.state.vibrate}
              onValueChange={async (value)=>{
                this.setState({vibrate:value});
                await persistAppParamToStorage({vibrate:value});
              }}
            />
          </View>
          <View style={{borderBottomWidth:1, borderColor:'#607D8B', width:Dimensions.get('window').width*0.9}}></View>
          <Text style={{fontSize:20, color:"#607D8B", fontFamily:'awesome', fontWeight:'bold', padding:10,}}>Generate QR Code</Text>
          <View style={{flexDirection:'row', marginTop:20, alignItems:'center', justifyContent:'center', width:Dimensions.get('window').width*0.9}}>
            <TextInput
              style={{height:40, borderColor:'gray', paddingHorizontal:5, borderWidth:1, width:Dimensions.get('window').width*0.8, borderRadius:5}}
              onChangeText={(text)=>{this.setState({qrCodeContent:text})}}
              value={this.state.qrCodeContent}
              placeholder="Enter Text"
              onEndEditing={async (event)=>{
                let qrText = event.nativeEvent.text;
                let appParam = await initAppParamFromStorage();
                if(appParam.hasOwnProperty('qrCodeSet') && Array.isArray(appParam.qrCodeSet)){
                  let qrCodeSet = new Set(appParam.qrCodeSet);
                  qrCodeSet.add(qrText);
                  await persistAppParamToStorage({qrCodeSet: Array.from(qrCodeSet)});
                }
              }}
              />
          </View>
        </View>

        {(this.state.qrCodeContent === '') ? null : (
          <View style={{flexDirection:'row', backgroundColor:'white', 
                      borderRadius:10,
                      marginTop:10,
                      padding:10,
                      shadowColor:'#000000',
                      shadowOffset:{width:4, height:8},
                      shadowRadius:5,
                      shadowOpacity:0.8,
                      alignItems:'center',
                      justifyContent:'center',
                      width:Dimensions.get('window').width*0.9,}}>
            <QRCode value={this.state.qrCodeContent} style={{flex:1}}/>
            <Text style={{flex:3, paddingHorizontal:4}}>{this.state.qrCodeContent}</Text>
          </View>
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#455A64'
  },
});
